const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const listEndpoints = require('express-list-endpoints')
const open = require('open');

const app = express();
const config = require('./webpack.shared.js');
const compiler = webpack(config);

const port = 3000;

// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(webpackDevMiddleware(compiler, {
	publicPath: config.output.publicPath,
}));

app.set('view engine', 'hbs');
app.set('views', './dist');
app.engine('html', require('hbs').__express); 

app.get('/', (_req, res) => {
	res.render('landing.html', {test: 2});
});

app.get('/endpoints', function (_req, res) {
	res.send(listEndpoints(app));
});

app.listen(port, function () {
	console.log(`Dev server started on port ${port}!`);
	open(`http://127.0.0.1:${port}/endpoints`);
});

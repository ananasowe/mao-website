const express = require('express');

const app = express();
const handlebars = require('hbs');
const moment = require('moment');
const db = require('./server/database');
const utils = require('./server/utils');

const port = 3000;

handlebars.registerHelper('notnull', (value) => (value != null));

app.set('view engine', handlebars.__express);
app.set('views', './dist');
app.engine('html', handlebars.__express); 
app.disable('x-powered-by');

//Block .html files for safety
app.get('/*.html$/', (_req, _res, next) => {
	next();
});

//app.use(/([a-z0-9\-\/\.]+)\.(js|css|png|jpg|gif|bmp)\/?$/, express.static('./dist'))
app.use(express.static('./dist'))

app.get('/', (_req, res) => {
	res.render('landing.html', {test: 2});
});

app.get('/home', (_req, res) => {
	res.render('home.html', {test: 2});
});

app.get('/news/:page?', (req, res) => {
	let page = req.params.page;
	let rpp = 10;
	if(!page || isNaN(page))
		page = 0;
	//res.render('news.html', {test: 2});
	db.getArticles(page, rpp+1).then(results => 
		res.render('news.html', { 
			articles: results.map(result => formatArticle(result, true)),
			pageForward: page > 0 ? Number(page) - 1 : null,
			pageBack: results.length > rpp ? Number(page) + 1 : null 
		})
	)
});

app.get('/article/:id', (req, res, next) => {
	db.getArticle(req.params.id).then(results => {
		if(results.length > 0) {
			let result = results[0];
			res.render('article.html', {
				article: formatArticle(result)
			});
		} else {
			next();
		}
	});
});

app.get('/statistics', (_req, res) => {
	res.render('statistics.html', {test: 2});
});

app.get('/login', (_req, res) => {
	res.render('login.html', {test: 2});
});

app.get('/articles', (_req, res) => {
	db.getArticles(0, 30).then(results => res.json(results.map(result => formatArticle(result))));
});

function formatArticle (article, strip = false) {
	return {
		id: article.id,
		author: article.author,
		date: moment.utc(article.date).format("DD.MM.YYYY"),
		name: article.name,
		content: utils.sanitizeHtml(strip ? article.content.match(/^((.*?)([.\n]|$)){0,10}/g) : article.content)
	};
};

app.listen(port, function () {
	console.log(`Web server started on port ${port}!`);
});

app.use(function (req, res, _next){
  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    res.render('404.html', { url: req.url });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');
});

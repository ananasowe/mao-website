const sanitize = require('sanitize-html');
const cheerio = require('cheerio');

module.exports = {
	escapeHtml: (string) => {
		return string
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
	},
	sanitizeHtml: (html) => sanitize(html, {
		allowedTags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'big', 'small', 'u', 'i', 'b', 's',
			'strong', 'em', 'strike', 'center' , 'abbr', 'code', 'img', 'a', 'ul' ,'ol', 'li', 'p', 'br'],
		disallowedTagsMode: 'discard',
		allowedAttributes: {
			'a': ['href', 'name', 'target'],
			'img': ['alt', 'src']
		},
		allowedIframeHostnames: ['www.youtube.com', 'www.imgur.com'],
		enforceHtmlBoundary: true
	}),
	isInteger: (variable) => {
		return (variable === parseInt(variable, 10));
	}
};

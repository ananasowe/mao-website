const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Model = Sequelize.Model;

const connection = new Sequelize('mariadb://admin:administrator@localhost:3306/web');

connection
	.authenticate()
	.then(() => {
		console.log('Database connection has been established successfully.');
	})
	.catch(err => {
		console.error('Unable to connect to the database:', err);
	});

const Article = connection.define('article', {
	/*id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
	},*/
	name: {
		type: Sequelize.STRING,
		allowNull: false
	},
	author: {
		type: Sequelize.STRING
	},
	date: {
		type: Sequelize.DATE
	},
	content: {
		type: Sequelize.TEXT
	},
	hidden: {
		type: Sequelize.BOOLEAN
	},
	archived: {
		type: Sequelize.BOOLEAN
	}
}, {
	//options
	timestamps: false
});

Article.sync({ force: false });
/*Article.sync({ force: false}).then(() => {
	return Article.create({
		name: "Drugi test",
		author: "Anana5",
		date: new Date(),
		content: "<big>Hello!</big>"
	});
});*/

module.exports = {
	getArticles: (page, itemsPerPage) => {
		return Article.findAll({ 
			where: { 
				hidden: { [Op.not]: true }, 
				archived: { [Op.not]: true } 
			}, 
			order: [['id', 'DESC']], 
			offset: page * itemsPerPage, 
			limit: itemsPerPage 
		});
	},
	getArticle: (identifier) => {
		return Article.findAll({
			where: { 
				id: identifier,
				archived: { [Op.not]: true }
			},
			limit: 1 
		});
	}
};

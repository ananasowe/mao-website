const path = require("path");
const webpackmerge = require("webpack-merge");
const shared = require("./webpack.shared");

module.exports = webpackmerge(shared, {
	mode: "development",
});

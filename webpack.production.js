const merge = require("webpack-merge");
const shared = require("./webpack.shared");

module.exports = merge(shared, {
	mode: "production",
	output: {
		filename: "lib.[contentHash].js",
	}
});

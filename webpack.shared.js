const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {

	// Path to your entry point. From this file Webpack will begin his work
	entry: {
		landingPage: './src/landing.js',
		homePage: './src/home.js',
		newsPage: './src/news.js',
		articlePage: './src/article.js',
		statisticsPage: './src/statistics.js',
		loginPage: './src/login.js'
	},

	plugins: [
		new CleanWebpackPlugin(),
		new MiniCssExtractPlugin({
		  filename: '[name].[contenthash].css',
		  chunkFilename: '[id].[contenthash].css',
		}),
		new HtmlWebpackPlugin({
		  inject: true,
		  minify: true,
		  chunks: ['landingPage'],
		  filename: 'landing.html',
		  template: './src/html/landing.html'
		}),
		new HtmlWebpackPlugin({
		  inject: true,
		  minify: true,
		  chunks: ['homePage'],
		  filename: 'home.html',
		  template: './src/html/home.html'
		}),
		new HtmlWebpackPlugin({
		  inject: true,
		  minify: true,
		  chunks: ['statisticsPage'],
		  filename: 'statistics.html',
		  template: './src/html/statistics.html'
		}),
		new HtmlWebpackPlugin({
		  inject: true,
		  minify: true,
		  chunks: ['newsPage'],
		  filename: 'news.html',
		  template: './src/html/news.html'
		}),
		new HtmlWebpackPlugin({
		  inject: true,
		  minify: true,
		  chunks: ['articlePage'],
		  filename: 'article.html',
		  template: './src/html/article.html'
		}),
		new HtmlWebpackPlugin({
		  inject: true,
		  minify: true,
		  chunks: ['loginPage'],
		  filename: 'login.html',
		  template: './src/html/login.html'
		}),
	],

	optimization: {
		splitChunks: {
			chunks: 'all',
			name: false
		}
	},

	module: {
		rules: [
			{
				test: /\.html/i,
				loader: 'html-loader',
			},
			{
				test: /\.css$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
				],
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'sass-loader'
				],
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: {
					loader: 'file-loader',
					options: {
						name: "[name].[ext]",
						outputPath: "imgs"
					}
				},
			},
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			}
		]
	},

	output: {
		path: path.resolve(__dirname, 'dist'),
		publicPath: '/',
		filename: '[name].[chunkhash].js',
		chunkFilename: '[name].[chunkhash].js'
	},

	// Default mode for Webpack is production.
	// Depending on mode Webpack will apply different things
	// on final bundle. For now we don't need production's JavaScript 
	// minifying and other thing so let's set mode to development
	mode: 'development'
};

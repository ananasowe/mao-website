const statistics = {
	data() {
		return {
			data: [
			],
			columns: [
				{
					field: 'lp',
					label: 'lp.',
					width: '40',
					numeric: true,
				},
				{
					field: 'player',
					label: 'Gracz',
				},
				{
					field: 'score',
					label: 'Wynik',
				},
				{
					field: 'date',
					label: 'Data osiągnięcia',
					centered: true,
				}
			],
			isEmpty: true
		}
	}
}

const app = new Vue(statistics)
app.$mount('#scores')

